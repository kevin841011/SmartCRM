﻿var UserId = "";
var trId = "";
var isAdd = true;

function EditUser(UserId) {  
    isAdd = false;

    clearForm(isAdd);

    $.ajax({
        url: "/System/GetUser",
        type: 'GET',
        cache: false,
        data: { UserId: UserId },
        success: function (result) {
            if (result.code == 0) {
                $("#UserName").val(result.data.UserName);
                $("#Phone").val(result.data.Phone);
                $("#Mobile").val(result.data.Mobile);
                $("#Email").val(result.data.Email);
                $("#UserId").val(result.data.UserId);
                $("#Memo").val(result.data.Memo);
                showUser();                
            }
            else {
                $("#errorMessage").text(result.message);
            }

        },
        error: function (e) {
            $("#errorMessage").text("查找用户信息失败");
        }
    });

    return false;
}

function DeleteUser(UserId) {
    if (!confirm('确定删除吗？')) {
        return;
    }
    $.ajax({
        url: "/System/DeleteUser",
        type: 'POST',
        cache: false,
        data: { UserId: UserId },
        success: function (result) {
            if (result.code == 0) {
                location.href = '/System/User';
            }
            else {
                alert(result.message);
            }

        },
        error: function (e) {
            alert('删除失败');
        }
    });

    return false;
}

function ResetPassword(UserId,UserName) {
    if (!confirm('确定要为用户 ' + UserName+'('+ UserId+') 重置密码吗？')) {
        return;
    }
    $.ajax({
        url: "/System/ResetPassword",
        type: 'POST',
        cache: false,
        data: { UserId: UserId },
        success: function (result) {
            if (result.code == 0) {
                $('#divPassword').dialog('open')
                $("#divPasswordContent").html(result.message);                
            }
            else {
                alert(result.message);
            }

        },
        error: function (e) {
            alert('密码重置失败');
        }
    });

    return false;
}


$(function () {

    $('#divUser').dialog({
        width: 500,
        autoOpen: false,
        modal: true,
        buttons: {
        }
    });

    $('#divPassword').dialog({
        width: 180,
        autoOpen: false,
        modal: true,
        buttons: {
        }
    });
    
    $('#divDepartment').dialog({
        width: 500,
        autoOpen: false,
        modal: true,
        buttons: {
        }
    });


    $('#divPermission').dialog({
        width: 520,
        autoOpen: false,
        modal: true,
        buttons: {
        }
    });

    $('#btnNew').click(function () {
        isAdd = true;
        clearForm(isAdd);
        showUser();
    });

    $('#btnCancel').click(function () {
        $('#divUser').dialog('close');
    });

    $("#btnAddDepartment").click(function () {
        MoveOptions("adepartment", "bdepartment");
    });

    $("#btnRemoveDepartment").click(function () {
        MoveOptions("bdepartment", "adepartment");
    });


    $("#btnSaveDepartment").click(function () {
        if (!confirm("确定要保存吗？")) {
            return;
        }
        var departments = [];
        $("#bdepartment option").each(function () {
            departments.push(this.value);
        });

        $.ajax({
            url: "/System/UpdateUserDepartment",
            type: 'POST',
            cache: false,
            data: { UserId: $("#UserId").val(), Department: JSON.stringify(departments) },
            success: function (result) {
                if (result.code == 0) {
                    closeDepartment();
                    location.href = "/System/User";
                }
                else {
                    $("#errorMessageDepartment").text(result.message);
                }
            },
            error: function (e) {
                $("#errorMessageDepartment").text("保存失败.");
            }
        });
    });

    $("#btnSavePermission").click(function () {
        if (!confirm("确定要保存吗？")) {
            return;
        }
        var permissions = [];
        $("#divPermission").find("input[type=checkbox]").each(function () {
            if ($(this).prop("checked")) {
                permissions.push(this.id);
            }
        });

        $.ajax({
            url: "/System/UpdateUserPermission",
            type: 'POST',
            cache: false,
            data: { UserId: $("#UserId").val(), Permission: JSON.stringify(permissions) },
            success: function (result) {
                if (result.code == 0) {
                    closePermission();
                    location.href = "/System/User";
                }
                else {
                    $("#errorMessagePermission").text(result.message);
                }
            },
            error: function (e) {
                $("#errorMessagePermission").text("保存失败.");
            }
        });
    });

    $('#btnSave').click(function () {

        if (isAdd) {
            if (checkRequired('divUser', 'errorMessage') == false) {
                return;
            }

            if ($("#UserId").val().indexOf(",") >= 0) {
                $("#UserId").addClass("redBorder");
                $("#errorMessage").text("登录名称不能包含逗号(,)");
                return;
            }
            $.ajax({
                url: "/System/AddUser",
                type: 'POST',
                cache: false,
                data: { UserId: $("#UserId").val(), UserName: $("#UserName").val(), Phone: $("#Phone").val(), Email: $("#Email").val(), Mobile: $("#Mobile").val(), Memo: $("#Memo").val() },
                success: function (result) {
                    if (result.code == 0) {
                        closeUser();
                        location.href = "/System/User";
                    }
                    else {
                        $("#errorMessage").text(result.message);
                    }
                },
                error: function (e) {
                    $("#errorMessage").text("保存失败.");
                }
            });
        }
        else {
            if (!checkRequired('divUser', 'errorMessage')) {
                return;
            }
            if (confirm('确定保存修改吗？')) {

                $.ajax({
                    url: "/System/UpdateUser",
                    type: 'POST',
                    cache: false,
                    data: { UserId: $("#UserId").val(), UserName: $("#UserName").val(), Phone: $("#Phone").val(), Email: $("#Email").val(), Mobile: $("#Mobile").val(), Memo: $("#Memo").val() },
                    success: function (result) {
                        if (result.code == 0) {
                            closeUser();
                            location.href = "/System/User";
                        }
                        else {
                            $("#errorMessage").text(result.message);
                        }
                    },
                    error: function (e) {
                        $("#errorMessage").text("保存失败.");
                    }
                });
            }
        }
    });
});

function showUser() {
    $('#divUser').dialog('open');
}

function closeUser() {
    $('#divUser').dialog('close');
}

function showDepartment() {
    $('#divDepartment').dialog('open');
}

function closeDepartment() {
    $('#divDepartment').dialog('close');
}

function showPermission() {
    $('#divPermission').dialog('open');
}

function closePermission() {
    $('#divPermission').dialog('close');
}


function EditDepartment(UserId) {
    $("#UserId").val(UserId);
    $.ajax({
        url: "/System/GetUserDepartment",
        type: 'GET',
        cache: false,
        data: { UserId: UserId },
        success: function (result) {
            if (result.code == 0) {
                $("#adepartment").empty();
                $("#bdepartment").empty();
                for (var i = 0; i < result.data.length; i++) {
                    if (result.data[i].AdditionalValue == "0") {
                        $("#adepartment").append("<option value='" + result.data[i].OptionValue + "'>" + result.data[i].OptionText + "</option>");
                    }
                    else {
                        $("#bdepartment").append("<option value='" + result.data[i].OptionValue + "'>" + result.data[i].OptionText + "</option>");
                    }
                }
                showDepartment();
            }
            else {
                $("#errorMessageDepartment").text(result.message);
            }

        },
        error: function (e) {
            $("#errorMessageDepartment").text("查找用户信息失败");
        }
    });
}

function EditPermission(UserId) {
    $("#UserId").val(UserId);
    $.ajax({
        url: "/System/GetUserPermission",
        type: 'GET',
        cache: false,
        data: { UserId: UserId },
        success: function (result) {
            if (result.code == 0) {
                console.log(result.data);
                $("#divPermission").find("input[type=checkbox]").attr("checked", false);
                for (var i in result.data) {
                    console.log($("#divPermission").find("input[id=" + result.data[i] + "]"));
                    $("#divPermission").find("input[id=" + result.data[i] + "]").attr("checked", true);
                }
                showPermission();
            }
            else {
                $("#errorMessagePermission").text(result.message);
            }

        },
        error: function (e) {
            $("#errorMessagePermission").text("查找用户信息失败");
        }
    });
}

function clearForm(isAdd) {
    $("input").removeClass("redBorder");  
    $("input[type=text]").val("");   
    $(".errorSpan").html("");
    if (isAdd) {
        $("#UserId").attr("disabled", false);
        $("#UserId").focus();
    }
    else {
        $("#UserId").attr("disabled", true);
        $("#UserName").focus();
    }
}

function MoveOptions(sourceId, destinationId) {
    var x = $("#" + sourceId+" option");
    var aDepartmentList = $("#" + sourceId);
    var departmentList = $("#"+destinationId);
    var msgs = "";
    x.each(function () {
        if (this.selected) {
            departmentList.prepend(this.outerHTML);
            $("#"+ sourceId + " option[value='" + this.value + "']").remove();
        }
    });
}