﻿var orderNo;

$(function () { 
        
    $('#divDetail').dialog({
        width: 950,
        autoOpen: false,
        modal: true,
        buttons: {
        }
    });

    $('#divPayment').dialog({
        width: 600,
        autoOpen: false,
        modal: true,
        buttons: {
        }
    });  

});

function ShowDetail(id) {

    $.ajax({
        url: "/Order/GetDetail",
        type: 'POST',
        cache: false,
        data: { OrderNo: id },
        success: function (result) {
            if (result.code == 0) {
                $("#dStatus").html(result.data.StatusDescription);               
                $("#dOrderNo").html(result.data.OrderNo);
                $("#dOrderName").html(result.data.OrderName);
                $("#dCustomer").html(result.data.CustomerShortName);
                $("#dContact").html(result.data.Contact);
                $("#dAmount").html(result.data.Amount);
                $("#dFactoryAmount").html(result.data.FactoryAmount);
                $("#dNumber").html(result.data.Number);
                $("#dExpectDeliveryDate").html(result.data.ExpectDeliveryDateString);
                $("#dCreatedTime").html(result.data.CreatedTimeString);
                $("#dTransactionDate").html(result.data.TransactionDateString);
                $("#dCreator").html(result.data.Creator);
                $("#dPaid").html(result.data.Paid);
                $("#dDetail").html(result.data.Detail);              
                $("#dMemo").html(result.data.Memo);
                $("#dQueryURL").html("");
                var a = $("<a target='_blank' href='" + result.data.QueryURL + "'>" + result.data.QueryURL + "</a>").appendTo($("#dQueryURL"));
              
                var t = $("#tdAttachment");
                t.html("");
                for (var r in result.data.Attachments) {
                    var a = $("<div title='点击下载'><img src='" + result.data.Attachments[r].Addition1 + "' alt=''/><a href='#' style='text-decoration:underline;' onclick=\"javascript:return DownLoadFile('" + result.data.Attachments[r].ItemValue + "');\">" + result.data.Attachments[r].ItemText + "</a>(" + result.data.Attachments[r].Addition2 + ")</div>");
                    a.appendTo(t);
                }

                $('#divDetail').dialog('open');
            }
            else {
                alert("获取信息失败.");
            }

        },
        error: function (e) {
            alert("获取信息失败.");
        }
    });
}

function DeleteOrder(id) {
    if (!confirm("确定删除吗?")) {
        return;
    }

    $.ajax({
        url: "/Order/Delete",
        type: 'POST',
        cache: false,
        data: { OrderNumber: id },
        success: function (result) {
            if (result.code == 0) {
                $("#tOrder").find("tr[id='" + id + "']").remove();
            }
            else if (result.code == 1){
                alert(result.message);
            }

        },
        error: function (e) {
            alert('删除失败');
        }
    });
}

function EditOrder(id) {
    location.href = "/Order/Edit?OrderNo=" + id;
}

function DownLoadFile(no) {
    location.href = "/Order/DownLoadAttachment?FileNo=" + no;
}

function Approve(no) {
    if (confirm("审核以前，请确保已经为订单设置了正确的金额和流程。\r是否确定？")) {        
        $.ajax({
            url: "/Order/Approve",
            type: 'POST',
            cache: false,
            data: { OrderNo: no },
            success: function (result) {
                if (result.code == 0) {
                    window.location.reload();
                }
                else if (result.code == 1) {
                    alert(result.message);
                }

            },
            error: function (e) {
                alert('审核失败');
            }
        });
    }
}