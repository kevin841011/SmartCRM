﻿$(function () {
    $("#btnSave").click(function () {
        if (checkRequired('divCompany', 'errorMessage') == false) {
            return false;
        }

        file = $("#logofile");

        if (file.val() != "") {
            var point = file.val().lastIndexOf(".");
         
            if (point <= 0) {
                $("#errorMessage").text("未知的文件扩展名");
                return false;
            }

            var type = file.val().substr(point).toLowerCase();
            console.log(type);
            if (!(type == ".jpg" || type == ".jpeg" || type == ".bmp" || type == ".png" || type == "gif")) {
                $("#errorMessage").text("图片格式仅支持jpg,bmp,png和gif");
                return false;
            }
        }

        if (!confirm("确定要保存吗?")) {
            return false;
        }

        return true;

    });
});

