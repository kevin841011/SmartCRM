﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.Web.Security;

namespace smartcrm.Utilities
{
    public class MD5Utility
    {        
        private static string randomChars = "bcdfghjkmpqrtvwxy2346789";
        public static string MD5Encrypt(string input)
        {
            return FormsAuthentication.HashPasswordForStoringInConfigFile(input, "MD5");
        }

        public static string GetRandomPassword(int passwordLen)
        {
            string password = string.Empty;
            int randomNum;
            Random random = new Random();
            for (int i = 0; i < passwordLen; i++)
            {
                randomNum = random.Next(randomChars.Length);
                password += randomChars[randomNum];
            }
            return password;
        }
    }
   
}