﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Text;
using System.Web.Routing;
using System.Configuration;

namespace System.Web.Mvc
{    
    public class PagerConfig
    {   
        public int CurrentPage { get; set; }
      
        public int PageSize { get; set; } 
     
        public int TotalPage { get { return (int)Math.Ceiling(TotalRecord / (double)PageSize); } }
            
        public int TotalRecord { get; set; }
       
        public string RecordUnit { get; set; }
       
        public string RecordName { get; set; }

        public PagerConfig()
        {
            CurrentPage = 1;
            PageSize = int.Parse(ConfigurationManager.AppSettings["PageSize"].ToString()); ;
            RecordUnit = "条";
            RecordName = "记录";
        }
    }
    
    public class PagerData<T> : List<T>
    {
        public PagerData(List<T> list, PagerConfig pagerConfig)
        {
            this.AddRange(list);
            Config = pagerConfig;
        }
        public PagerData(List<T> list, int currentPage, int pageSize, int totalRecord)
        {
            this.AddRange(list);
            Config.CurrentPage = currentPage;
            Config.PageSize = pageSize;
            Config.TotalRecord = totalRecord;
        }
        public PagerData(List<T> list, int currentPage, int pageSize, int totalRecord, string recordUnit, string recordName)
        {
            this.AddRange(list);
            Config.CurrentPage = currentPage;
            Config.PageSize = pageSize;
            Config.TotalRecord = totalRecord;
            Config.RecordUnit = recordUnit;
            Config.RecordName = recordName;
        }

        public PagerConfig Config { get; set; }
    }
}

namespace System.Web.Mvc.Html
{
    public static class PagerExtensions
    {
        public static MvcHtmlString Pager(this HtmlHelper htmlHelper, string actionName, string controllerName, RouteValueDictionary routeValues, PagerConfig pageConfig, string ctrlId, string cssClass, int digitalLinkNum, bool showTotalRecord, bool showCurrentPage, bool showTotalPage, bool showSelect, bool showInput)
        {
            UrlHelper _url = new UrlHelper(htmlHelper.ViewContext.RequestContext);
            StringBuilder _strBuilder = new StringBuilder("<div id=\"" + ctrlId + "\" class=\"" + cssClass + "\">");
            if (showTotalRecord) _strBuilder.Append("共" + pageConfig.TotalRecord + pageConfig.RecordUnit + pageConfig.RecordName + " ");
            if (showCurrentPage) _strBuilder.Append("每页" + pageConfig.PageSize + pageConfig.RecordUnit + " ");
            if (showTotalPage) _strBuilder.Append("第" + pageConfig.CurrentPage + "页/共" + pageConfig.TotalPage + "页 ");

            //首页链接
            if (pageConfig.CurrentPage > 1)
            {
                routeValues["page"] = 1;
                _strBuilder.Append("<a class=\"linkbtn\" href=\"" + _url.Action(actionName, controllerName, routeValues) + "\">首页</a>");
            }
            else _strBuilder.Append("<span class=\"btn\">首页</span>");
            //上一页
            if (pageConfig.CurrentPage > 1)
            {
                routeValues["page"] = pageConfig.CurrentPage - 1;
                _strBuilder.Append("<a class=\"linkbtn\" href=\"" + _url.Action(actionName, controllerName, routeValues) + "\">上一页</a>");
            }
            else _strBuilder.Append("<span class=\"btn\">上一页</span>");
            //数字导航开始
            int _startPage, _endPage;
            //总页数少于要显示的页数，页码全部显示
            if (digitalLinkNum >= pageConfig.TotalPage) { _startPage = 1; _endPage = pageConfig.TotalPage; }
            else//显示指定数量的页码
            {
                int _forward = (int)Math.Ceiling(digitalLinkNum / 2.0);
                if (pageConfig.CurrentPage > _forward)//起始页码大于1
                {
                    _endPage = pageConfig.CurrentPage + digitalLinkNum - _forward;
                    if (_endPage > pageConfig.TotalPage)//结束页码大于总页码结束页码为最后一页
                    {
                        _startPage = pageConfig.TotalPage - digitalLinkNum;
                        _endPage = pageConfig.TotalPage;

                    }
                    else _startPage = pageConfig.CurrentPage - _forward;
                }
                else//起始页码从1开始
                {
                    _startPage = 1;
                    _endPage = digitalLinkNum;
                }
            }
            //向上…
            if (_startPage > 1)
            {
                routeValues["page"] = _startPage - 1;
                _strBuilder.Append("<a class=\"linkbatch\" href=\"" + _url.Action(actionName, controllerName, routeValues) + "\">…</a>");
            }
            //数字
            for (int i = _startPage; i <= _endPage; i++)
            {
                if (i != pageConfig.CurrentPage)
                {
                    routeValues["page"] = i;
                    _strBuilder.Append("<a class=\"linknum\" href=\"" + _url.Action(actionName, controllerName, routeValues) + "\">" + i.ToString() + "</a>");
                }
                else
                {
                    _strBuilder.Append("<span class='currentnum'>" + i.ToString() + "</span>");
                }
            }
            //向下…
            if (_endPage < pageConfig.TotalPage)
            {
                routeValues["page"] = _endPage + 1;
                _strBuilder.Append("<a class=\"linkbatch\" href=\"" + _url.Action(actionName, controllerName, routeValues) + "\">…</a>");
            }
            ////数字导航结束
            //下一页和尾页
            if (pageConfig.CurrentPage < pageConfig.TotalPage)
            {
                routeValues["page"] = pageConfig.CurrentPage + 1;
                _strBuilder.Append("<a class=\"linkbtn\" href=\"" + _url.Action(actionName, controllerName, routeValues) + "\">下一页</a>");
                routeValues["page"] = pageConfig.TotalPage;
                _strBuilder.Append("<a class=\"linkbtn\" href=\"" + _url.Action(actionName, controllerName, routeValues) + "\">尾页</a>");
            }
            else _strBuilder.Append("<span class=\"btn\">下一页</span><span class=\"btn\">尾页</span>");
            //显示页码下拉框
            if (showSelect)
            {
                routeValues["page"] = "-nspageselecturl-";
                _strBuilder.Append(" 跳转到第<select id=\"nspagerselect\" data-url=\"" + _url.Action(actionName, controllerName, routeValues) + "\">");
                for (int i = 1; i <= pageConfig.TotalPage; i++)
                {
                    if (i == pageConfig.CurrentPage) _strBuilder.Append("<option selected=\"selected\" value=\"" + i + "\">" + i + "</option>");
                    else _strBuilder.Append("<option value=\"" + i + "\">" + i + "</option>");
                }
                _strBuilder.Append("</select>页");
                _strBuilder.Append("<script type=\"text/javascript\">$(\"#" + ctrlId + " #nspagerselect\").change(function () { location.href = $(\"#" + ctrlId + " #nspagerselect\").attr(\"data-url\").replace(\"-nspageselecturl-\", $(\"#" + ctrlId + " #nspagerselect\").val());});</script>");
            }
            //显示页码输入框
            if (showInput)
            {
                routeValues["page"] = "-nspagenumurl-";
                _strBuilder.Append("转到第<input id=\"nspagernum\" type=\"text\" data-url=\"" + _url.Action(actionName, controllerName, routeValues) + "\" />页");
                _strBuilder.Append("<script type=\"text/javascript\">$(\"#" + ctrlId + " #nspagernum\").keydown(function (event) {if (event.keyCode == 13) location.href = $(\"#" + ctrlId + " #nspagernum\").attr(\"data-url\").replace(\"-nspagenumurl-\", $(\"#" + ctrlId + " #nspagernum\").val()); });</script>");
            }
            _strBuilder.Append("</div>");
            return MvcHtmlString.Create(_strBuilder.ToString());
        }
    }
}