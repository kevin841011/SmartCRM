﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Cryptography;
using System.Text;
using smartcrm.Utilities;
using smartcrm.Models;
using System.IO;

namespace smartcrm.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return Redirect("/MySpace/Index");
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult Error(string id)
        {
            string message = string.Empty;
            switch (id)
            {
                case "1":
                    message = "无权访问，请联系管理员。";
                    break;
                case "2":
                    message = "未找到实体。";
                    break;
                default:
                    message = "访问时发生错误，请稍后重试。<br/>如果多次出现该错误，请联系管理员。";
                    break;
            }
            ViewBag.Message = message;
            return View();
        }

        public ActionResult State()
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

            if (Session["State"] == null)
            {
                Session["State"] = "1";
            }
            return View();
        }

        [HttpGet]
        [Authorization]
        public ActionResult ReportBug()
        {
            return View();
        }

        [HttpPost]
        [Authorization]
        public ActionResult ReportBug(Bug c)
        {
            HttpFileCollectionBase files = Request.Files;
            string newBugNo = GetNewIdNumber(IdNumberType.Bug);
            List<IdNumber> newNos = new List<IdNumber>();
            newNos.Add(new IdNumber() { No = newBugNo, Type = IdNumberType.Bug });
            c.BugNo = newBugNo;
            c.CompanyNo = CompanyNo;
            c.Creator = GetLoginUser().UserName;
            c.CreateTime = DateTime.Now;
            c.Status = 1; //
            foreach (string f in files)
            {
                HttpPostedFileBase file = files[f];
                if (!string.IsNullOrEmpty(file.FileName) && file.ContentLength > 0)
                {
                    string newAttachmentNo = GetNewIdNumber(IdNumberType.BugAttachment);
                    newNos.Add(new IdNumber() {  Type= IdNumberType.BugAttachment, No = newAttachmentNo});

                    BugAttachment attachment = new BugAttachment();
                    attachment.BugAttachmentNo = newAttachmentNo;

                    string realFileName = Path.GetFileName(file.FileName);
                    string fileDirectory = Path.Combine(BUG_ATTACHMENT_PATH, CompanyNo);
                    fileDirectory = Path.Combine(fileDirectory, newBugNo);
                    string fileName = Path.Combine(fileDirectory, newAttachmentNo + Path.GetExtension(file.FileName));

                    attachment.FileName = realFileName;
                    attachment.FilePath = fileName;
                    attachment.BugNo = newBugNo;

                    try
                    {
                        if (!Directory.Exists(fileDirectory))
                        {
                            Directory.CreateDirectory(fileDirectory);
                        }
                        file.SaveAs(fileName);
                    }
                    catch
                    {
                        AddErrorMessage(string.Format("Bug attachment upload failed. FileName:{0}, CompanyNo:{1}", realFileName, CompanyNo));
                    }

                    Context.BugAttachment.AddObject(attachment);
                }
            }

            Context.Bug.AddObject(c);
            try
            {
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                PutbackNos(newNos);
                throw ex;
            }

            return Redirect("/MySpace/Index");
        }
    }
}
