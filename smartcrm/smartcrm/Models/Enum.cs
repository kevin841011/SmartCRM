﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace smartcrm.Models
{
    public enum IdNumberType
    {
        Order = 1,
        Customer = 2,
        Factory = 3,
        WorkFlow = 4,
        WorkFlowStep = 5,
        Department = 6,
        OrderStatus = 7,
        ContributionRatio = 8,
        PaymentType = 9,
        Contact = 10,
        OrderFlow = 11,
        User = 12,
        OrderAttachment = 13,
        OrderMemo = 14,
        Bug = 15,
        BugAttachment = 16
    }

    public enum ExceptionType
    {
        NoPermission = 1,
        NoEntityFound = 2,
        Other = 99
    }
}
