﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using System.ComponentModel;
using System.Data.Objects.DataClasses;

namespace smartcrm.Models
{
    public class PortalUser
    {
        public string LoginId { get; set; }
        public string LoginName { get; set; }
        public string CustomerShortName { get; set; }
        public string Password { get; set; }
        public string CompanyNo { get; set; }
        public string CustomerNo { get; set; }
    }

    public class PortalOrderList
    {
        public List<OrderInfo> UnApprovedOrders { get; set; }
        public List<OrderInfo> ApprovedOrders { get; set; }

        public string FileSizeLimitation { get; set; }
    }
} 
