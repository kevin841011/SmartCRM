﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Routing;

namespace smartcrm.Models
{
    public class CustomerList:PagerModel
    {
        public List<Customer> Customers { get; set; }
        public string Keywords { get; set; }
    }  
}
