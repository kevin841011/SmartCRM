﻿var ContactNo = "";
var trId = "";
var isAdd = true;

function EditContact(ContactNo) {  
    isAdd = false;

    clearForm(isAdd);

    $.ajax({
        url: "/Contact/GetContact",
        type: 'GET',
        cache: false,
        data: { ContactNo: ContactNo },
        success: function (result) {
            if (result.ContactNo != "") {
                $("#ContactName").val(result.ContactName);
                $("#Phone").val(result.Phone);
                $("#Mobile").val(result.Mobile);
                $("#Email").val(result.Email);
                $("#CustomerNo").val(result.CustomerNo);
                $("#SearchKey").val(result.SearchKey);
                $("#ContactNo").val(result.ContactNo);
                $("#QQ").val(result.QQ);
                $("#Other").val(result.Other);
                showContact();
                $("#ContactName").focus();
            }

        },
        error: function (e) {
        }
    });

    return false;
}

function DeleteContact(ContactNo) {
    if (!confirm('确定删除吗？')) {
        return;
    }
    $.ajax({
        url: "/Contact/Delete",
        type: 'POST',
        cache: false,
        data: { ContactNo: ContactNo },
        success: function (result) {
            if (result.code == 0) {
                location.href = location.href;
            }
            else {
                alert(result.message);
            }

        },
        error: function (e) {
            alert('删除失败');
        }
    });

    return false;
}

$(function () {

    $('#divContact').dialog({
        width: 500,
        autoOpen: false,
        modal: true,
        buttons: {
        }
    });

    $('#btnNew').click(function () {
        isAdd = true;
        clearForm(isAdd);
        $("#CustomerNo").val($("#sCustomerNo").val());
        showContact();
        $("#ContactName").focus();
    });

    $('#btnCancel').click(function () {
        $('#divContact').dialog('close');
    });


    $('#btnSave').click(function () {

        if (isAdd) {
            if (checkRequired('divContact', 'errorMessage') == false) {
                return;
            }

            if (!confirm("确定保存吗？")) {
                return;
            }

            $.ajax({
                url: "/Contact/Add",
                type: 'POST',
                cache: false,
                data: $('form').serialize(),
                success: function (result) {
                    if (result.code == 0) {
                        closeContact();
                        location.href = location.href;
                    }
                    else {
                        $("#errorMessage").text(result.message);
                    }
                },
                error: function (e) {
                    $("#errorMessage").text("保存失败.");
                }
            });
        }
        else {
            if (!checkRequired('divContact', 'errorMessage')) {
                return;
            }
            if (confirm('确定保存修改吗？')) {
                $.ajax({
                    url: "/Contact/Update",
                    type: 'POST',
                    cache: false,
                    data: $('form').serialize(),
                    success: function (result) {
                        if (result.code == 0) {
                            closeContact();
                            location.href = location.href;
                        }
                        else {
                            $("#errorMessage").text(result.message);
                        }
                    },
                    error: function (e) {
                        $("#errorMessage").text("保存失败.");
                    }
                });
            }
        }
    });
});

function showContact() {
    $('#divContact').dialog('open');
}

function closeContact() {
    $('#divContact').dialog('close');
}

function clearForm(isAdd) {
    $("input").removeClass("redBorder");  
    $("input[type=text]").val("");   
    $(".errorSpan").html("");
    if (isAdd) {       
        $("#ContactName").focus();
    }
    else {
        $("#ContactName").focus();
    }
}