﻿$(function () {
    $("#sb").attr("readonly", "readonly");
    $("#se").attr("readonly", "readonly");
    $("#sb").datepicker({ dateFormat: 'yy-mm-dd' });
    $("#se").datepicker({ dateFormat: 'yy-mm-dd' });

    $("#sb,#se").dblclick(function () {
        $(this).val("");
    });

    $("#btnExport").click(function () {
        if (checkRequired('dSearch', '') == false) {
            return false;
        }  
        location.href = "/Order/GetCustomerStatementPdf?sc=" + $("#sc").val() + "&sb=" + $("#sb").val() + "&se=" + $("#se").val();
    });
});
function checkField() {
    if (checkRequired('dSearch', '') == false) {
        return false;
    }
    else {
        return true;
    }
    }
